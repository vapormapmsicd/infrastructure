# Description
This directory contains a set of ***terraform*** codes that enable deploying an infrastructure (production environment) on OpenStack. The proposed infrastructure consists of Bastion and Master nodes sitting in the same sub-network, but each has its security group. The figure below represents a block diagram representation of the proposed architecture. The provisioning of the hosts is done with ***ansible***, ***cloud-init*** and ***terraform remote-exec***. Microk8s, a minimal lightweight Kubernetes is installed on the Master node. A gitlab shell runner is configured on the bastion node. However, the developer can register more than two runners. The specifications of the nodes are given below.

> - vCPU: 4
> - Memory: 8 GB
> - Disk: 20 GB 
> - Operating system: Ubuntu

<img src=./img/vapormap-openstack.png width="300" height="300" title="Block diagram of the proposed architecture" style="margin-left: 150pt;"/>

# How to deploy
Below are the steps to deploy and set up the infrastructure on Openstack for the Vapomap application

1. **Download the project directory**:
```shell
URL=https://gitlab.com/gbazack/cloud-orchestration.git
git clone $URL deployment
cd deployment/terraform/vapormap/
```

2.  **Login to your openstack project via CLI**:
```shell
echo "your_openstack_password" | source <your_openrc_file.sh>
```

3.  **Update terraform variables in**:<br>
The terraform variables to update are found in the file `terraform.tfvars`. We recommend to update values of the following variables:

> - `ssh_key`: name of the SSH keypair
> - `local-path-to-ssh-key`: local path to the SSH keypair
> - `remote-path-to-ssh-key`: path to the SSH keypair on the ***bastion*** node
> - `gitlab-runner-token`: registration token for the gitlab runner
> - `gitlab-runner-name`: tag of the gitlab runner
> - `gitlab-runner-description`: description of the gitlab runner
> - `gitlab-runner-executor`: gitlab runner executor


4.  **Deploy the infrastructure**:<br>
The  following command is used to initialize a working directory containing Terraform configuration files, and to create an execution plan, which lets you preview the changes that Terraform plans to make to your infrastructure.

```shell
make init
```

The command below applies the planned changes to each resource using the Openstack and other relevant infrastructure provider's API.

```shell
make deploy
```

5. **Manage the kubernetes infrastructure**:<br>
As mentionned earlier, a minimal lightwieght kubernetes is installed and configured on the ***Master*** node. Only the ***Bastion*** node enables you to access and administer your kubernetes infrastructure. The connection to the bastion is via SSH and can be carried out as follows: 

- **Update the environment variables**:<br>
These environment variables are used for the ssh connection. We have `USER_NAME`, the user's name of the bastion OS (`ubuntu` by default) and `PATH_TO_SSH_KEY`, the local path to SSH keypair required for the connection. The variables are found in `scripts/env.sh` file.

-  **Connect to the bastion**:<br>
Among the commands below, the first enable to load the environment variables and the second command generates the required ssh configurations. Finally, the final command enables to login to the bastion node.
> ***When using the command `make ssh-config`, please check and update your local `~/.ssh/config`***

```shell
source ./scripts/env.sh
make ssh-config
make connect
```