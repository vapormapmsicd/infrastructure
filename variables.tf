variable "username" {
    type    = string
    default = "ubuntu"
}

variable "nb_instance" {
    type    = number
    default = 1
}

variable "ssh_key" {
    type    = string
    default = "terraform-ssh"
}

variable "instance_flavor" {
    type    = string
    default = "m1.small"
}

variable "remote-path-to-ssh-key" {
    type    = string
    default = "/home/user/.ssh/terraform-ssh.pem"
}

variable "local-path-to-ssh-key" {
    type    = string
    default = "/home/ubuntu/.ssh/terraform-ssh.pem"
}

variable "gitlab-runner-token" {
    type     = string
    default  = "diW6CeM4pszQWsgNrsns"
}

variable "gitlab-runner-name" {
    type     = string
    default  = "openstack-shell-runner"
}

variable "gitlab-runner-description" {
    type     = string
    default  = "openstack-shell-runner"
}

variable "gitlab-runner-executor" {
    type     = string
    default  = "shell"
}