git clone https://gitlab.com/gbazack/kubernetes.git /home/ubuntu/kubernetes
echo "Installing ansible"
sudo apt install -y ansible
echo "Executing ansible playbooks"
cd /home/ubuntu/ansible/
ansible-playbook -i hosts --private-key "${SSH_KEY}" playbooks/set-proxy.yaml
ansible-playbook -i hosts --private-key "${SSH_KEY}" playbooks/install.yaml
ansible-playbook -i hosts --private-key "${SSH_KEY}" playbooks/config.yaml
ansible-playbook -i hosts --private-key "${SSH_KEY}" playbooks/enable-addons.yaml
ansible-playbook -i hosts --private-key "${SSH_KEY}" playbooks/copy.yaml